import csv
import datetime
import sys
import os


def addOutRowCount(row):
	addOutRowCount.count += 1
	return(row | {'TimeOfDay':str(datetime.timedelta(seconds=addOutRowCount.count))})
addOutRowCount.count = 0

TD_TRANSACTIONS_FOLDER = 'td_transactions'
OUTPUT_FOLDER = 'out'
OUTPUT_FILENAME = os.path.join(OUTPUT_FOLDER, 'out.csv')
SECURITY_TRANSFER_COST_BASE_FILE = 'security_transfer_cost_base.csv'
seucrity_transfer_cost_base_dict = {}
if not os.path.exists(OUTPUT_FOLDER):
	os.mkdir(OUTPUT_FOLDER)
#Read security transfer cost base from file into dictionary
with open(SECURITY_TRANSFER_COST_BASE_FILE, 'r') as infile:
	reader = csv.DictReader(infile)
	for row in reader:
		seucrity_transfer_cost_base_dict[row['TRANSACTION ID']] = row['Cost Per Share']

with open(OUTPUT_FILENAME, 'w', newline='', encoding='utf-8') as outfile:
	outFieldnames = ['Symbol', 'Name', 'DisplaySymbol', 'Exchange', 'Portfolio', 'Currency', 'Quantity', 'Cost Per Share', 'Cost Basis Method', 'Commission', 'Date', 'TimeOfDay', 'PurchaseFX', 'Type', 'Notes']
	writer = csv.DictWriter(outfile, fieldnames=outFieldnames)
	writer.writeheader()
	entries = os.listdir(TD_TRANSACTIONS_FOLDER)
	for infileName in entries:
		if not ".csv" in infileName:
			continue
		with open(os.path.join(TD_TRANSACTIONS_FOLDER, infileName), 'r') as infile:
			reader = csv.DictReader(infile)
			for row in reader:
				if 'END OF FILE' in row['DATE']:
					continue
				date_time_obj = datetime.datetime.strptime(row['DATE'], '%m/%d/%Y')
				date_str = str(date_time_obj.date()) + " GMT+0800"
				outRow = {'Portfolio':'TD', 'Date': date_str, 'Notes': row['DESCRIPTION'], 'Currency':'USD', 'Notes': row['DESCRIPTION'], 'Cost Basis Method':1, 'Commission':row['COMMISSION']}
				if 'Bought' in row['DESCRIPTION']:
					writer.writerow(addOutRowCount(outRow | {'Type':'Sell', 'Symbol':'USD=CASH', 'Quantity':float(row['AMOUNT']) * -1, 'Cost Per Share':0}))
					writer.writerow(addOutRowCount(outRow | {'Type':'Buy', 'Symbol':row['SYMBOL'], 'Quantity':row['QUANTITY'], 'Cost Per Share':row['PRICE'], 'Exchange': 'NMS'}))
				elif 'Sold' in row['DESCRIPTION']:
					writer.writerow(addOutRowCount(outRow | {'Type':'Sell', 'Symbol':row['SYMBOL'], 'Quantity':row['QUANTITY'], 'Cost Per Share':row['PRICE'], 'Exchange': 'NMS'}))
					writer.writerow(addOutRowCount(outRow | {'Type':'Buy', 'Symbol':'USD=CASH', 'Quantity':row['AMOUNT'], 'Cost Per Share':0}))
				elif ('SECURITY TRANSFER FEE' in row['DESCRIPTION'] or
					  'CASH MOVEMENT OF INCOMING ACCOUNT TRANSFER' in row['DESCRIPTION'] or
					  'WIRE INCOMIN' in row['DESCRIPTION'] or
					  'FREE BALANCE INTEREST ADJUSTMENT' in row['DESCRIPTION'] or
					  'CLIENT REQUESTED ELECTRONIC FUNDING RECEIPT (FUNDS NOW)' in row['DESCRIPTION']):
					writer.writerow(addOutRowCount(outRow | {'Type':'Buy', 'Symbol':'USD=CASH', 'Quantity':row['AMOUNT'], 'Cost Per Share': 1}))
				elif ('QUALIFIED DIVIDEND' in row['DESCRIPTION'] or
					  'ORDINARY DIVIDEND' in row['DESCRIPTION']):
					writer.writerow(addOutRowCount(outRow | {'Type':'Dividend', 'Symbol':row['SYMBOL'], 'Quantity':row['AMOUNT'], 'Cost Per Share': 0}))
					writer.writerow(addOutRowCount(outRow | {'Type':'Buy', 'Symbol':'USD=CASH', 'Quantity':row['AMOUNT'], 'Cost Per Share': 0}))
				elif 'ACH' in row['DESCRIPTION']:
					if float(row['AMOUNT']) > 0:
						# When ACH USD to TD account
						writer.writerow(addOutRowCount(outRow | {'Type':'Buy', 'Symbol':'USD=CASH', 'Quantity':row['AMOUNT'], 'Cost Per Share': 1}))
					else:
						# I have not ACH TD USD to other account yet, so i am not sure if this case will be valid
						print('Warning: not handle transaction data [', row['DESCRIPTION'], ']')
				elif 'W-8 WITHHOLDING' in row['DESCRIPTION']:
					writer.writerow(addOutRowCount(outRow | {'Type':'Dividend', 'Symbol':row['SYMBOL'], 'Quantity':row['AMOUNT'], 'Cost Per Share': 0}))
					writer.writerow(addOutRowCount(outRow | {'Type':'Sell', 'Symbol':'USD=CASH', 'Quantity':float(row['AMOUNT']) * -1, 'Cost Per Share': 0}))
				elif 'TRANSFER OF SECURITY OR OPTION IN' in row['DESCRIPTION']:
					#check if we have cost base of this security transfer transaction
					if not row['TRANSACTION ID'] in seucrity_transfer_cost_base_dict:
						print('Error: Abort due to missing security transfer cost base information of transaction ', row['TRANSACTION ID'])
						sys.exit()
					writer.writerow(addOutRowCount(outRow | {'Type':'Buy', 'Symbol':row['SYMBOL'], 'Quantity':row['QUANTITY'], 'Cost Per Share':seucrity_transfer_cost_base_dict[row['TRANSACTION ID']]}))
				else:
					print('Warning: not handle transaction data [', row['DESCRIPTION'], ']')
print('done')
os.system("pause")
