# TD Transactions Converter

Convert TD transaction data for importing account holding to My Stocks Portfolio App https://apps.apple.com/us/app/my-stocks-portfolio-widget/id923544282

Steps
1. Install python3.9
2. Download this project to your computer
3. Download the TD transactions data from TD website and put it in folder td_transactions
4. If you have security transfer transaction, fill the cost base in security_transfer_cost_base.  You can find the cost base in TD website My account -> Cost Basis -> Position Transaction History.
5. Run the python script e.g python TD_transaction_parser.py
6. The script will about if you did not fill the required cost base for security transffering transactions.
7. The script will also showing transaction message which is not handle by this script
8. The result csv file will be generated in out folder.  Import that csv file to My Stock Profolio App.
